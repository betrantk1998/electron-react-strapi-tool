import React, { Component } from 'react'
import 'antd/dist/antd.css';
import { Input, Select, Button, message } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { URLDEV, URLPROD } from '../constains/apiConfig';
import axios from 'axios';
import { CSVLink } from "react-csv";

const { Option } = Select;


class Export extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bearToken: localStorage.getItem("bearToken") ? localStorage.getItem("bearToken") : null,
            sellerCode: null,
            type: null,
            envi: 'dev',
            data: null,
            fileName: 'export'
        }
        this.handChangeEnvi = this.handChangeEnvi.bind(this);
        this.handChangeFileName = this.handChangeFileName.bind(this);
        this.handChangeSellerCode = this.handChangeSellerCode.bind(this);
        this.handChangeToken = this.handChangeToken.bind(this);
        this.handChangeType = this.handChangeType.bind(this);
        this.handleExport = this.handleExport.bind(this)

    }
    handChangeEnvi(value) {
        this.setState({
            envi: value
        })
    }
    handChangeSellerCode(value) {
        this.setState({
            sellerCode: value.target.value
        })
    }
    handChangeType(value) {

        this.setState({
            type: value.target.value
        })
    }
    handChangeToken(value) {
        this.setState({
            bearToken: value.target.value
        })
        localStorage.setItem("bearToken", value.target.value)
    }
    handChangeFileName(value) {
        this.setState({
            fileName: value.target.value
        })
    }
    async handleExport() {
        const { bearToken, sellerCode, type, envi, fileName } = this.state;
        if (bearToken && sellerCode && type) {
            const URL = envi === 'dev' ? URLDEV : URLPROD
            try {
                const data = await axios.get(
                    `${URL}/content-manager/explorer/${type}?sellerCode=${sellerCode.toUpperCase()}&_limit=1000000&_start=0&_sort=_id%3AASC&source=content-manager`, {
                    headers: {
                        Authorization: `Bearer ${bearToken}`,
                    }
                }
                )
                if (data.status === 200) {
                    this.setState({
                        data: data
                    })
                    message.success('Get data success, pls click Download button to download file')
                }
            }
            catch (err) {
                message.error('Get data failed, please try again')

            }
        }else{
            message.warn('Please input all field')

        }
    }
    render() {
        const { bearToken, sellerCode, type, envi, fileName, data } = this.state;
     
        return (
            <div className='container'>
                <Input.Group compact >
                    <div className="form-export" style={{ display: 'flex' }}>

                        <Select className='export-item' style={{ width: 400 }} defaultValue="dev" value={envi} onChange={this.handChangeEnvi}>
                            <Option value="dev">DEVELOP</Option>
                            <Option value="prod">PRODUCTION</Option>
                        </Select>

                        <Input className='export-item' style={{ width: 400 }} onChange={this.handChangeSellerCode} value={sellerCode} placeholder="Seller Code" defaultValue="FCV" />

                        <Input className='export-item' style={{ width: 400 }} onChange={this.handChangeType} value={type} placeholder="Type Export" defaultValue="shipto" />
                        <Input className='export-item' style={{ width: 400 }} onChange={this.handChangeFileName} value={fileName} placeholder="File Name" defaultValue="fileExport" />
                        <TextArea className='export-item' style={{ width: 400, height: 100 }} onChange={this.handChangeToken} value={bearToken} placeholder="Token Bear" />
                        <Button className="btn-export export-item" onClick={this.handleExport}>Export File CSV</Button>
                        {data && <CSVLink data={data.data} filename={`${fileName}.csv`}>
                        <Button className="btn-export export-item" style={{backgroundColor: '#ff8c00'}}>Download Here</Button>
                        </CSVLink>}
                    </div>
                </Input.Group>
            </div>
        )
    }

}

export default Export