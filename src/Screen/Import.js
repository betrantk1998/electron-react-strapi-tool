import React, { Component } from 'react';
import { Input, Select, Button, message } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { URLDEV, URLPROD } from '../constains/apiConfig';
import Papa from "papaparse";
import axios from 'axios';

const { Option } = Select;
class Import extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bearToken: localStorage.getItem("bearToken") ? localStorage.getItem("bearToken") : null,
            envi: 'dev',
            fileCSV: null
        }
        this.handChangeEnvi = this.handChangeEnvi.bind(this);
        this.handChangeFile = this.handChangeFile.bind(this);
        this.handChangeToken = this.handChangeToken.bind(this);
        this.handleImport = this.handleImport.bind(this)

    }
    handChangeEnvi(value) {
        this.setState({
            envi: value
        })
    }
    handChangeFile(value) {
        this.setState({
            fileCSV: value.target.files
        })

    }
    handChangeToken(value) {
        this.setState({
            bearToken: value.target.value
        })
        localStorage.setItem("bearToken", value.target.value)
    }

    handleImport() {
        const { bearToken, envi, fileCSV } = this.state;

        const URL = envi === 'dev' ? URLDEV : URLPROD
        const URLIMPORT = `${URL}/content-manager/explorer/vehicle`

        if (fileCSV && bearToken) {
            const config = {
                headers: {
                    Authorization: `Bearer ${bearToken}`,
                }
            }
            Papa.parse(fileCSV[0], {
                header: true,
                complete: function (result) {
                    for (let entry of result.data) {
                        if (entry._id !== '') {
                            try {
                                axios.put(`${URLIMPORT}/${entry._id}/?source=content-manager`, entry, config).then(resp => {
                                    message.success('Import file success!')
                                })
                                    .catch(err => {
                                        // Handle Error Here
                                        message.error('Import file failed, please try again')

                                    });
                            }
                            catch (err) {
                                // Handle Error Here
                                message.error('Import file failed, please try again')
                            }
                        }

                    }


                }
            });
        }

    }

    render() {
        const { bearToken, envi } = this.state;

        return (
            <div className='container'>
                <Input.Group compact >
                    <div className="form-export" style={{ display: 'flex' }}>

                        <Select className='export-item' style={{ width: 400 }} defaultValue="dev" value={envi} onChange={this.handChangeEnvi}>
                            <Option value="dev">DEVELOP</Option>
                            <Option value="prod">PRODUCTION</Option>
                        </Select>

                        <Input multiple={false} type={'file'} accept=".csv,.xlsx,.xls" className='export-item' style={{ width: 400 }} onChange={this.handChangeFile} />
                        <TextArea className='export-item' style={{ width: 400, height: 100 }} onChange={this.handChangeToken} value={bearToken} placeholder="Token Bear" />
                        <Button className="btn-export export-item" onClick={this.handleImport}>Import File CSV</Button>
                    </div>
                </Input.Group>
            </div>
        );
    }
}

Import.propTypes = {

};

export default Import;